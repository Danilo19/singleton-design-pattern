package com.example.singletondesignpattern

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        Singleton.instance
        Singleton.instance.sampleName="Singleton Pattern implementado en Kotlin"
        println(Singleton.instance.sampleName)
    }
}
